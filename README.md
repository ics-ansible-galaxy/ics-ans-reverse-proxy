ics-ans-reverse-proxy
===================

Ansible playbook to install reverse-proxy.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
